output "target" {
  description = "redirected to site"
  value       = "${var.redirect}"
}

output "fqdn" {
  description = "fqdn built of the zone domain and r53name"
  value       = "${aws_route53_record.www.fqdn}"
}
