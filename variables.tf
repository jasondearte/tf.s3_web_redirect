variable "web_domain" {
  type        = "string"
  description = "domain for the web page: www.example.com or example.com"
}

variable "root_domain" {
  type        = "string"
  description = "root domain for the web page: example.com"
}

variable "zone_id" {
  type        = "string"
  description = "route53 zone_id for the base fqdn"
}

variable "tags" {
  type        = "map"
  description = "tags for your resources"
  default     = {}
}

variable "redirect" {
  type        = "string"
  description = "redirect to another site, i.e. https://other.site.example.com"
}

variable "cert_arn" {
  type        = "string"
  description = "ARN of the certificate for the FQDN"
}

variable "price_class" {
  type        = "string"
  description = "PriceClass_100, PriceClass_200, PriceClass_All, [aws](https://aws.amazon.com/cloudfront/pricing/?sc_channel=PS&sc_campaign=acquisition_US&sc_publisher=google&sc_medium=ACQ-R%7CPS-GO%7CBrand%7CSU%7CDelivery%7CCloudFront%7CUS%7CEN%7CText&sc_content=sitelink&sc_detail=cloudfront%20pricing&sc_category=cloudfront&sc_segment=Pricing&sc_matchtype=p&sc_country=US&s_kwcid=AL!4422!3!278339312312!p!!g!!cloudfront%20pricing&ef_id=UVE2RgAAAfpUAJle:20180825042502:s)"
}

variable "ttl_default" {
  type        = "string"
  description = "how long Cloudfront will cache the page"
  default     = ""
}

variable "ssl_protocols" {
  type        = "list"
  description = "limit which TLS protocols you want to support"
  default     = ["TLSv1.2"]
}
