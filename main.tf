/**
* # s3_web_redirect
*
* s3 bucket to host a web site redirection page
*
* Cloudfront will host TLS and route web traffic to the bucket containing the redirect
*
* # Example
*
*      # BTW: DON'T PIN TO MASTER, pick a tag so you're code doesn't break if if the interface changes
*
*      module "web_redir" {
*        source        = "git::https://gitlab.com/jasondearte/tf.s3_web_redirect.git?ref=master"
*        web_domain    = "www.example.com"
*        root_domain   = "example.com"
*        zone_id       = "${data.aws_route53_zone.zone.zone_id}"
*        tags          = "${local.common_tags}"
*        cert_arn      = "${module.dv_cert.arn}"
*        price_class   = "${local.price_class}"
*        ttl_default   = "${local.ttl_default}"
*        ssl_protocols = "${local.ssl_protocols}"
*        redirect      = "example.com"
*      }
*
* # Interface
*/

resource "aws_route53_record" "www" {
  # for a root domain, the name value needs to be empty
  name    = "${var.root_domain == var.web_domain ? "" : var.web_domain}"
  type    = "A"
  zone_id = "${var.zone_id}"

  alias = {
    name                   = "${aws_cloudfront_distribution.dist.domain_name}"
    zone_id                = "${aws_cloudfront_distribution.dist.hosted_zone_id}"
    evaluate_target_health = false
  }
}

resource "aws_s3_bucket" "www" {
  bucket = "${var.web_domain}"
  acl    = "public-read"
  tags   = "${var.tags}"

  policy = <<POLICY
{
  "Version":"2012-10-17",
  "Statement":[
    {
      "Sid":"AddPerm",
      "Effect":"Allow",
      "Principal": "*",
      "Action":["s3:GetObject"],
      "Resource":["arn:aws:s3:::${var.web_domain}/*"]
    }
  ]
}
POLICY

  website {
    redirect_all_requests_to = "${var.redirect}"
  }
}

resource "aws_cloudfront_distribution" "dist" {
  enabled         = true
  is_ipv6_enabled = true
  aliases         = ["${var.web_domain}"]
  price_class     = "${var.price_class}"
  tags            = "${var.tags}"

  origin {
    domain_name = "${aws_s3_bucket.www.website_endpoint}"
    origin_id   = "${var.web_domain}"

    custom_origin_config {
      http_port              = "80"
      https_port             = "443"
      origin_protocol_policy = "http-only"
      origin_ssl_protocols   = "${var.ssl_protocols}"
    }
  }

  default_cache_behavior {
    viewer_protocol_policy = "redirect-to-https"
    target_origin_id       = "${var.web_domain}"
    compress               = true
    allowed_methods        = ["GET", "HEAD"]
    cached_methods         = ["GET", "HEAD"]
    min_ttl                = 0
    default_ttl            = "${var.ttl_default}"
    max_ttl                = 86400

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    acm_certificate_arn = "${var.cert_arn}"
    ssl_support_method  = "sni-only"
  }
}
