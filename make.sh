#!/usr/bin/env bash

echo "Making the readme"

make_readme() {
    MODULE=$1

    terraform fmt $MODULE

    terraform-docs md $MODULE   > $MODULE/README.md
    echo "# Terraform Version" >> $MODULE/README.md
    terraform --version        >> $MODULE/README.md
}

make_readme "."
