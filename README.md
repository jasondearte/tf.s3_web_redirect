# s3_web_redirect

s3 bucket to host a web site redirection page

Cloudfront will host TLS and route web traffic to the bucket containing the redirect

# Example

     # BTW: DON'T PIN TO MASTER, pick a tag so you're code doesn't break if if the interface changes

     module "web_redir" {
       source        = "git::https://gitlab.com/jasondearte/tf.s3_web_redirect.git?ref=master"
       web_domain    = "www.example.com"
       root_domain   = "example.com"
       zone_id       = "${data.aws_route53_zone.zone.zone_id}"
       tags          = "${local.common_tags}"
       cert_arn      = "${module.dv_cert.arn}"
       price_class   = "${local.price_class}"
       ttl_default   = "${local.ttl_default}"
       ssl_protocols = "${local.ssl_protocols}"
       redirect      = "example.com"
     }

# Interface


## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| cert_arn | ARN of the certificate for the FQDN | string | - | yes |
| price_class | PriceClass_100, PriceClass_200, PriceClass_All, [aws](https://aws.amazon.com/cloudfront/pricing/?sc_channel=PS&sc_campaign=acquisition_US&sc_publisher=google&sc_medium=ACQ-R%7CPS-GO%7CBrand%7CSU%7CDelivery%7CCloudFront%7CUS%7CEN%7CText&sc_content=sitelink&sc_detail=cloudfront%20pricing&sc_category=cloudfront&sc_segment=Pricing&sc_matchtype=p&sc_country=US&s_kwcid=AL!4422!3!278339312312!p!!g!!cloudfront%20pricing&ef_id=UVE2RgAAAfpUAJle:20180825042502:s) | string | - | yes |
| redirect | redirect to another site, i.e. https://other.site.example.com | string | - | yes |
| root_domain | root domain for the web page: example.com | string | - | yes |
| ssl_protocols | limit which TLS protocols you want to support | list | `<list>` | no |
| tags | tags for your resources | map | `<map>` | no |
| ttl_default | how long Cloudfront will cache the page | string | `` | no |
| web_domain | domain for the web page: www.example.com or example.com | string | - | yes |
| zone_id | route53 zone_id for the base fqdn | string | - | yes |

## Outputs

| Name | Description |
|------|-------------|
| fqdn | fqdn built of the zone domain and r53name |
| target | redirected to site |

# Terraform Version
Terraform v0.11.8

